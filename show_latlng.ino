#include "MC20_Common.h"
#include "MC20_Arduino_Interface.h"
#include "MC20_GPRS.h"
#include "MC20_GNSS.h"
#include <Adafruit_NeoPixel.h>

#define RGBPIN       10
#define LED_NUM      1
#define RGBLED_POWER_PIN  6

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(LED_NUM, RGBPIN, NEO_GRB + NEO_KHZ800);

GNSS gnss = GNSS();

GPRS gprs = GPRS();

const char apn[10] = "CMNET";

//const char apn[50] = "wap.vodafone.co.uk";
//const char apn[10] = "UNINET";
const char URL[100] =  "122.166.50.174";
//String http = "GET /?id=&lat=0.000000&lon=0.ss000000&timestamp=1561630635&hdop=0&altitude=0&speed=0 HTTP/1.0\n\r\n\r";
int port = 5055;

int ret = 0;


void red(int times) {
  // Red LED Blink
  for(int i=0;i<times;i++){
    pixels.setPixelColor(0, pixels.Color(255, 0, 0));
    pixels.show();  
    delay(500);
    pixels.setPixelColor(0, pixels.Color(0, 0, 0));
    pixels.show();
    delay(500);
  }
}

void blue() {
  // Blue LED Blink
  pixels.setPixelColor(0, pixels.Color(0, 0, 100));
  pixels.show();
  delay(700);
  pixels.setPixelColor(0, pixels.Color(0, 0, 0));
  pixels.show();
}

void green() {
  // Green LED Blink
  pixels.setPixelColor(0, pixels.Color(0, 255, 0));
  pixels.show();
  delay(700);
  pixels.setPixelColor(0, pixels.Color(0, 0, 0));
  pixels.show();
}

void setup() {
  pinMode(RGBLED_POWER_PIN, OUTPUT);
  digitalWrite(RGBLED_POWER_PIN, HIGH);
  pixels.begin(); 
  
  SerialUSB.begin(115200);
  
  gnss.Power_On();
  SerialUSB.println("\n\rGNSS Power On.");

  while (!gnss.open_GNSS(GNSS_DEFAULT_MODE)) {
    delay(1000);
  }

  SerialUSB.println("GNSS Working.");
  
  gprs.Power_On();
  
  if (!(ret = gprs.init(apn))) {
    SerialUSB.println("GPRS init error: ");
    SerialUSB.println(ret);
    SerialUSB.println("Exiting...");
    red(2);
    //exit(0);
  }
  SerialUSB.println("\n\rGPRS Connected.");
  
  if(!gprs.join()){
    SerialUSB.println("GPRS join error!\nExiting...");
    red(3);
    // exit(0);
  }
  SerialUSB.print("\n\rIP: ");
  SerialUSB.print(gprs.ip_string);
  SerialUSB.println("\n\rGPRS Joined.");
  
}

void loop() {
  
  if (gnss.getCoordinate()) {
    String lat = String(gnss.str_latitude);
    String lng = String(gnss.str_longitude);
    char http_cmd[200];

    // GPS Data Received
    blue();

    SerialUSB.print("GNSS: ");
    SerialUSB.print(gnss.str_longitude);
    SerialUSB.print(",");
    SerialUSB.println(gnss.str_latitude);
 
    String http = "GET /?id=123456&lat=" + lat + "&lon=" + lng + " HTTP/1.0\n\r\n\r";

    http.toCharArray(http_cmd, 200);

    SerialUSB.println(http_cmd);

    if (gprs.connectTCP(URL, port)) {
      gprs.sendTCPData(http_cmd);
      SerialUSB.println("Data Sent To Server!");
      green();
      //close tcp connection
      gprs.closeTCP();
    } else {
      SerialUSB.println("TCP Connect Error!");
      // Error
      red(5);
    }
    
  }
  else {
    SerialUSB.println("GPS Error!");
    red(1);
  }

  delay(5000);
}
